#define MYNAME "mk_FIFO"
#include<sys/types.h>
#include<sys/stat.h>
#include<stdio.h>
#include<ctype.h>
#include<fcntl.h>
#include<stdlib.h>
#include<unistd.h>

using namespace std;


int main(int argc, char*argv[])
{
	int salir=1;
	mkfifo("/tmp/loggerfifo",0777);
	int fd=open("/tmp/loggerfifo",O_RDONLY);
	while(salir)
	{
		char buff[200];
		read(fd,buff,sizeof(buff));
		printf("%s\n",buff);
		if(buff[0]=='0')
		{
		salir=0;
		printf("He cerrado el juego\n");
		}
	};
		
	close(fd);

	unlink("/tmp/loggerfifo");
	
	return 0;
}	
