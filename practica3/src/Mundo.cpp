// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "Mundo.h"
#include "glut.h"

#include <stdlib.h>
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<error.h>

#include<sys/stat.h>
#include<ctype.h>
#include<fcntl.h>
#include<unistd.h>



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

char*proyeccion;

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	
	//Cierre de la tubería
 	char cierre[2];
 	cierre[0]='0';
 	write(tuberia,cierre,strlen(cierre));
 	close(tuberia);
 	//Cierre de la proyección de memoria
	munmap(proyeccion,sizeof(MemComp));

}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	for(unsigned i=0;i<num_pelotas;i++)
		lista_esferas[i].Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	bool continua = true;
	for(int i=0;i<num_pelotas && continua;i++) {
		lista_esferas[i].Mueve(0.025 + ((puntos1+puntos2)/100));
		int n;
		for(n=0;n<paredes.size();n++)
		{
			paredes[n].Rebota(lista_esferas[i]);
			paredes[n].Rebota(jugador1);
			paredes[n].Rebota(jugador2);
		}

		jugador1.Rebota(lista_esferas[i]);
		jugador2.Rebota(lista_esferas[i]);
		if(fondo_izq.Rebota(lista_esferas[i]))
		{
			lista_esferas[i].centro.x=0;
			lista_esferas[i].centro.y=rand()/(float)RAND_MAX;
			lista_esferas[i].velocidad.x=2+2*rand()/(float)RAND_MAX;
			lista_esferas[i].velocidad.y=2+2*rand()/(float)RAND_MAX;
			puntos2++;
				num_pelotas++;
			}
			//Implementación del mensaje de tubería
			char cad[200];
			sprintf(cad,"jugador 2 marca 1 punto, lleva %d",puntos2);
			write(tuberia,cad,strlen(cad)+1);
			if ( num_pelotas < 4) {
				continua = false;
			
		}

		if(fondo_dcho.Rebota(lista_esferas[i]))
		{
			lista_esferas[i].centro.x=0;
			lista_esferas[i].centro.y=rand()/(float)RAND_MAX;
			lista_esferas[i].velocidad.x=-2-2*rand()/(float)RAND_MAX;
			lista_esferas[i].velocidad.y=-2-2*rand()/(float)RAND_MAX;
			puntos1++;

			//Implementación del mensaje de tubería
			char cad[200];
			sprintf(cad,"jugador 1 marca 1 punto, lleva %d",puntos1);
			write(tuberia,cad,strlen(cad)+1);
			if ( num_pelotas < 4) {
				continua = false;
				num_pelotas++;
			}
		}
	}

	//Implementación sobre el bot
	
	//Recibimos con un switch case el valor de la acción que se proyecta desde el bot en el neuvo fichero proyectado.
	switch(pMemComp->accion)
	{
		case 1: 
			if(jugador1.y1 <5.0){     //jugador1.velocidad.y=4;break;

				jugador1.velocidad.y=1;
				jugador1.y1 +=jugador1.velocidad.y;
				jugador1.y2 +=jugador1.velocidad.y;

			}
			break;
		case -1: if(jugador1.y2 >-5.0){

			jugador1.velocidad.y=-1; //jugador1.velocidad.y=-4;break;
			jugador1.y1 +=jugador1.velocidad.y;
			jugador1.y2 +=jugador1.velocidad.y;

		}

			break;
		default: break;
	}
	//Tambien debemos actualizar los valores reales en nuestra proyección del fichero
	for(int i=0;i<4;i++) {
		pMemComp->esferas[i]=lista_esferas[i];
	}
	pMemComp->raqueta1=jugador1;
	
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
	case 'a'://jugador1.velocidad.x=-1;break;
		if(jugador1.x1 >-6.0){

			jugador1.velocidad.x=-1; 
			jugador1.x1 +=jugador1.velocidad.x;
			jugador1.x2 +=jugador1.velocidad.x;
		}
		break;

	case 'd'://jugador1.velocidad.x=1;break;
		if(jugador1.x1 <-2.0){

			jugador1.velocidad.x=1; //jugador1.velocidad.y=-4;break;
			jugador1.x1 +=jugador1.velocidad.x;
			jugador1.x2 +=jugador1.velocidad.x;
		}
		break;
	case 's':
		if(jugador1.y2 >-5.0){

			jugador1.velocidad.y=-1; //jugador1.velocidad.y=-4;break;
			jugador1.y1 +=jugador1.velocidad.y;
			jugador1.y2 +=jugador1.velocidad.y;

		}

		break;
	case 'w':
		if(jugador1.y1 <5.0){     //jugador1.velocidad.y=4;break;

			jugador1.velocidad.y=1;
			jugador1.y1 +=jugador1.velocidad.y;
			jugador1.y2 +=jugador1.velocidad.y;

		}
		break;
	case 'j'://jugador1.velocidad.x=-1;break;
		if(jugador2.x1 >2.0){

			jugador2.velocidad.x=-1; 
			jugador2.x1 +=jugador2.velocidad.x;
			jugador2.x2 +=jugador2.velocidad.x;
		}
		break;

	case 'l'://jugador1.velocidad.x=1;break;
		if(jugador2.x1 <6.0){

			jugador2.velocidad.x=1; //jugador1.velocidad.y=-4;break;
			jugador2.x1 +=jugador2.velocidad.x;
			jugador2.x2 +=jugador2.velocidad.x;
		}
		break;

	case 'k':
		if(jugador2.y2 >-5.0){

			jugador2.velocidad.y=-1; //jugador1.velocidad.y=-4;break;
			jugador2.y1 +=jugador2.velocidad.y;
			jugador2.y2 +=jugador2.velocidad.y;

		}

		break;
	case 'i':
		if(jugador2.y1 <5.0){     //jugador1.velocidad.y=4;break;

			jugador2.velocidad.y=1;
			jugador2.y1 +=jugador2.velocidad.y;
			jugador2.y2 +=jugador2.velocidad.y;

		}
		break;
			
	//case 'l':jugador2.velocidad.y=-4;break;
	//case 'o':jugador2.velocidad.y=4;break;

	}
}

void CMundo::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	num_pelotas = 1;

	//Informacion Sobre el Logger
 	tuberia=open("/tmp/loggerfifo",O_WRONLY);

	//Fichero proyectado en memoria
	int file=open("/tmp/datosBot.txt",O_RDWR|O_CREAT|O_TRUNC, 0777); //Creacion del fichero
	write(file,&MemComp,sizeof(MemComp)); //Escribimos en el fichero
	proyeccion=(char*)mmap(NULL,sizeof(MemComp),PROT_WRITE|PROT_READ,MAP_SHARED,file,0); 	 	 	//Asignamos al puntero de proyeccion el lugar donde esta proyectado el fichero
	close(file); //Cerramos el fichero
	pMemComp=(DatosMemCompartida*)proyeccion; 
	pMemComp->accion=0; //De inicio, no queremos que haya ninguna acción.
}
